/****OBJETOS (Poderia fazer com classe tvlz mas nn aprendi ainda)****/
let Matriz1 = {
    numLinhas: undefined,
    numColunas: undefined,
    matriz: [],
    pedirTamanho: () => {
        Matriz1.numLinhas = prompt(`Quantas linhas tera a 1ª matriz?`);
        Matriz1.numColunas = prompt("E quantas colunas?");
    },
    preencherMatriz: () => {
        matrizTemp = []
        for (var i = 0; i < Matriz1.numLinhas; i++) {
            var lista = []
            for (var j = 0; j < Matriz1.numColunas; j++) {
                var num = prompt(`Digite o número da posição ${i}x${j}:`)
                parseInt(num)
                lista.push(num)
            }
            Matriz1.matriz.push(lista)
        }
    }
}

let Matriz2 = {
    numLinhas: undefined,
    numColunas: undefined,
    matriz: [],
    pedirTamanho: () => {
        Matriz2.numLinhas = prompt(`Quantas linhas tera a 2ª matriz?`);
        Matriz2.numColunas = prompt("E quantas colunas?");
    },
    preencherMatriz: () => {
        matrizTemp = []
        for (var i = 0; i < Matriz2.numLinhas; i++) {
            var lista = []
            for (var j = 0; j < Matriz2.numColunas; j++) {
                var num = prompt(`Digite o número da posição ${i}x${j}:`)
                parseInt(num)
                lista.push(num)
            }
            Matriz2.matriz.push(lista)
        }
    }
}

function multiplicarMatrizes(m1, m2) {
    // Número de colunas final = número de colunas da m1
    // Número de linhas final = número de linhas da m1
    var matrizFinal = []
    for(var k = 0; k < m1.numLinhas; k++){
        var lista = []
        for (var i = 0; i < m1.numLinhas; i++) {
            var soma = 0
            for (var j = 0; j < m1.numColunas; j++) {
                soma += m1.matriz[i][j] * m2.matriz[j][i]
            }
            lista.push(soma)
        }
        matrizFinal.push(lista)
    }
    return matrizFinal
}

console.log("Olá");

// Pedir o tamanho delas
Matriz1.pedirTamanho()
Matriz2.pedirTamanho()

while (Matriz1.numColunas != Matriz2.numLinhas) {
    console.log("O número de colunas da 1ª tem que ser igual ao número de linhas da 2ª!")
    Matriz1.pedirTamanho()
    Matriz2.pedirTamanho()
}

// Preencher as matrizes
Matriz1.preencherMatriz()
Matriz2.preencherMatriz()
console.log(Matriz1.matriz)
console.log(Matriz2.matriz)

// Multiplicar as matrizes
let matrizFinal = multiplicarMatrizes(Matriz1, Matriz2)

console.log(matrizFinal)



